; Age History

	B  main

born	DEFB "you were born in \0"
were	DEFB "you were \0"
in	DEFB " in \0"
are	DEFB "you are \0"
this	DEFB " this year\n\0"
	ALIGN

present DEFW 2005	; int present = 2005
birth DEFW 1959		; int birth = 1959
year DEFW 0			; int year
age DEFW 1			; int age = 1

main

	; this code does System.out.println("you were born in " + birth);
	ADR R0, born
	SVC 3
	LDR R0, birth ; make sure this will work!
	SVC 4
	MOV R0, #10
	SVC 0

	;year = birth + 1
	LDR R0, birth	;loading birth to R0
	ADD R0, R0, #1  ;year = year + 1
	STR R0, year	;storing R0 to year
	
	LDR R1, present	;loading present year to R1
	loop 
	
	CMP R0, R1      ; while (year != present) {
	BEQ skip		;if equal end the loop

	; this code does System.out.println("you were " + age + " in " + year);
	ADR R0, were
	SVC 3
	LDR R0, age ; make sure this will work!
	SVC 4
	ADR R0, in
	SVC 3
	LDR R0, year ; make sure this will work!
	SVC 4
	MOV R0, #10
	SVC 0

	;   year = year + 1;
	LDR R0, year
	ADD R0, R0, #1
	STR R0, year

	;   age = age + 1;
	LDR R0, age
	ADD R0, R0, #1
	STR R0, age

	LDR R0, year ; realoading year to R0
	B loop	; }

	; this code does System.out.println("you are " + age + "this year");
	skip ADR R0, are
	SVC 3
	LDR R0, age ; make sure this will work!
	SVC 4
	ADR R0, this
	SVC 3

	SVC 2 ; stop
