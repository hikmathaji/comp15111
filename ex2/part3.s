; Hello Someone program - version 1

	B main

hello	DEFB	"Hello \0"
goodbye	DEFB	"and good-bye!\n\0"
	ALIGN

main	ADR	R0, hello	; System.out.print("Hello ");

	SVC 	3	; printing Hello 
	teker 		; loop starts here
	SVC 1 		; input a character to R0
	SVC 0		; output the character in R0
	CMP R0, #10 ; while (R0 != 10) {// translate to ARM code
	ADREQ R0, goodbye ; adding goodbye to R0 
	SVCEQ 3		; if input is return print goodbye
	SVCEQ 2		; if input is return terminate the program
 	B teker		; repeat loop
