; Hello Someone program - version 2
	B main
hello	DEFB	"Hello \0" ; defining hello
goodbye	DEFB	" and good-bye!\n\0" ; defining goodbye
	ALIGN		; align
main	ADR	R0, hello	; System.out.print("Hello ");
	SVC 	3
next	; while (true) { // loop starts
	SVC	1		; input a character to R0
	CMP R0, #10 ; if (R0 == 10) {// translate to ARM code
	BNE skip	; 
	ADR	R0, goodbye 	;   System.out.println(" and good-bye!");
	SVC	3
	SVC  	2		;   stop the program
	skip ; }// translate to ARM code
	SVC	0		; output the character in R0
	B	next		; } //while
